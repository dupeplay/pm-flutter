import 'package:flutter/material.dart';
import 'package:pm/Constants.dart';
import 'package:pm/Dashboard/DashboardComponents.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Login extends StatefulWidget{

  @override
  _LoginState createState()=> _LoginState();
}

class _LoginState extends State<Login>{

  // Keys
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();

  // Form Elements
  var _userEmail;
  var _userPassword;

  // 
  bool _autoValidate = false;
  bool _isLoading = false;
  var _errorText = '';

  Future<dynamic> checkLoggedIn() async {

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    if(sharedPreferences.getInt('userId') != null){

        Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardComponents()));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    checkLoggedIn();
    super.initState();
  }

  @override
  Widget build(BuildContext context){

    return Scaffold(
      body: Center(
        child: _isLoading
        ? CircularProgressIndicator()
        : Container(
          decoration: insertBg(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 40.0),
                  child: Text("Log In to your account", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                ),
              ),
            
              Container(
                child: Text(_errorText, style:TextStyle(color:Colors.redAccent)),
              ),

              Form(
              key: _loginFormKey,
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    // Form Field Email/Username
                    Container(
                      // margin: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 30.0),
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: TextFormField(
                        validator: (_email){
                          if(_email.isEmpty){
                            return "E-mail is required field";
                          }
                        },
                        decoration: InputDecoration(
                          labelText: "E-mail",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          filled: true,
                          fillColor: Colors.green[50],
                        ),
                      onSaved: (_email) => _userEmail = _email,
                      ),
                    ),

                    Container(
                      // margin: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 30.0),
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      child: TextFormField(
                        validator: (_password){
                          if(_password.isEmpty){
                            return "Password is required field";
                          }
                        },
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: "Password",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          filled: true,
                          fillColor: Colors.green[50],
                        ),
                      onSaved: (_password) => _userPassword = _password,
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                      alignment: Alignment.center,
                      child: Text("Forget Password ?", style: TextStyle(fontSize: 15.0, color: Colors.green),),
                    ),

                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      alignment: Alignment.center,
                      child: ButtonTheme(
                        child: RaisedButton(
                          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
                          child: Text("Log In", style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold),),
                          onPressed: (){

                            login();
                          }
                        )
                      ),
                    ),
                  ],
                ),
              ),

                Row(
                mainAxisAlignment:  MainAxisAlignment.center,
                children: <Widget>[

                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                    child: Text("Privacy Policy"),
                  ),

                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                    child: Text("Terms & Conditions"),
                  ),

                  Container(
                    padding: EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                    child: Text("About App"),
                  ),

                ],
              ),

            ],
          ),
        ),
      ),
    );
  }

  login()async{

    if(_loginFormKey.currentState.validate()){
      _loginFormKey.currentState.save();

      var email = _userEmail;
      var password = _userPassword;

      Future<dynamic> checkLogin() async{

        var loginUrl = baseUrl+"signin/login";
        
        http.Response response = await http.post(

          loginUrl,
          body: {
            'apikey': apikey,
            'email': email,
            'password': password,
          }
        );

        var loginResponse = json.decode(response.body);

          // print(loginResponse['response']);


        if(loginResponse['status'] == 'success') {


          // Shared Preferences
          SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

          sharedPreferences.setInt('userId', int.parse(loginResponse['response']['id']));
          sharedPreferences.setString('fname', loginResponse['response']['first_name']);
          sharedPreferences.setString('lname', loginResponse['response']['last_name']);
          sharedPreferences.setString('userType', loginResponse['response']['user_type']);
          sharedPreferences.setString('email', loginResponse['response']['email']);
          sharedPreferences.setString('image', loginResponse['response']['image']);
          sharedPreferences.setString('jobTitle', loginResponse['response']['job_title']);
          sharedPreferences.setString('accessToken', loginResponse['response']['token']);

          // print(sharedPreferences.setString('image', loginResponse['response']['image']));

          // print(sharedPreferences.get('userId'));
          Navigator.push(context, MaterialPageRoute(builder: (context)=> DashboardComponents()));
        }

        else{

          setState(() {
            _isLoading = false;
            _errorText = loginResponse["message"];
          });
        }


      }

      setState(() {
        _isLoading = true;
        checkLogin();
      });

    }

    else{

      _autoValidate = true;
    }

  }

}