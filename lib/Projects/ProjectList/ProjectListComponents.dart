import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:pm/Constants.dart';
import 'package:pm/DrawerApp.dart';
import 'package:pm/Components.dart';
import 'ProjectListBody.dart';


class ProjectListComponents extends StatefulWidget{

  @override
  _ProjectListComponentsState createState()=> _ProjectListComponentsState();
}

class _ProjectListComponentsState extends State<ProjectListComponents>{

  GlobalKey<ScaffoldState>_scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){

    return MaterialApp(
      theme: appTheme(),
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("All Projects", style: TextStyle(color:Colors.white),),
          leading: IconButton(
            icon: Icon(Icons.apps, color: Colors.white,),
            onPressed: ()=>_scaffoldKey.currentState.openDrawer(),
          ),
          actions: <Widget>[
            Components(),
          ],
        ),
        drawer: DrawerApp(),
        body: ProjectListBody(),
      ),
    );
  }
}