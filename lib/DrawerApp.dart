import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Dashboard/DashboardComponents.dart';


class DrawerApp extends StatefulWidget{

  @override
  _DrawerAppState createState()  => _DrawerAppState();

  }

class _DrawerAppState extends State<DrawerApp>{

  int userId;
  var fullName;
  var userEmail;
  var userType;
  var userDisplay;
  var userJob;
  var userAvtar;

  Future<dynamic> getUser() async{

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    setState(() {
      userId = sharedPreferences.getInt('userId');
      fullName = sharedPreferences.getString('fname')+' '+sharedPreferences.getString('lname');
      userEmail = sharedPreferences.getString('email');
      userType = sharedPreferences.getString('userType');
      userJob = sharedPreferences.getString('jobTitle');
      userDisplay = sharedPreferences.getString('image');
      userAvtar = sharedPreferences.getString('fname')[0] + sharedPreferences.getString('lname')[0];

      // print(userDisplay);
    });
    
  }

  @override
  void initState() {
    // TODO: implement initState
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context){


    return Drawer(
      child: ListView(
        children:<Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(fullName, style: TextStyle(color:Colors.white),),
            accountEmail: Text(userEmail, style: TextStyle(color:Colors.white),),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.white,
              child: 
              (userDisplay != null)
              ? Image.network(userDisplay, fit: BoxFit.fill,)
              : Text(userAvtar.toUpperCase()),
            ),
          ),
          ListTile(
            title: Text('Dashboard', style: TextStyle(color:Colors.green[400]),),
            trailing: Icon(Icons.dashboard, color: Colors.green[400],),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardComponents()));
            },
          ),

          ListTile(
            title: Text('Customers', style: TextStyle(color:Colors.green[400])),
            trailing: Icon(Icons.people),
            onTap: (){
              // Navigator.of(context).pushNamed('/Leads');
              // Navigator.push(context, MaterialPageRoute(builder: (context) => CustomerList()));
            },

          ), 
          ListTile( 
            title: Text('Leads', style: TextStyle(color:Colors.green[400])),
            trailing: Icon(Icons.people),
            onTap: (){
              // Navigator.of(context).pushNamed('/Leads'); //comment
              // Navigator.push(context, MaterialPageRoute(builder: (context) => ListLeads()));
            },
          ),

          // Expansion Tile Used for Shops
          // ExpansionTile(
          //   title: Text('Shop'),
          //   trailing: Icon(Icons.arrow_drop_down),
          //   children: <Widget>[

          //     ListTile(
          //       title: Text('Products', style: TextStyle(fontSize:12.0),),
          //       trailing: Icon(Icons.fastfood),
          //       onTap: (){
          //         // Navigator.of(context).pushNamed('/Products');
          //       },
          //     ),
          //     // Divider(
          //     //   color:Colors.blueGrey.shade200,
          //     // ),
          //     ListTile(
          //       title: Text('Services', style: TextStyle(fontSize:12.0),),
          //       trailing: Icon(Icons.hot_tub),
          //       onTap: (){
          //         // Navigator.of(context).pushNamed('/Services');
          //       },
          //     ),
          //   ],
          // ),


          ListTile(
            title: Text('Invoices', style: TextStyle(color:Colors.green[400])),
            trailing: Icon(Icons.account_balance_wallet),
            onTap: (){
              // Navigator.push(context, MaterialPageRoute(builder: (context) => InvoiceList()));
            },
          ),

          ListTile(
            title: Text('Transactions', style: TextStyle(color:Colors.green[400])),
            trailing: Icon(Icons.attach_money),
            onTap: (){
              // Navigator.push(context, MaterialPageRoute(builder: (context) => Transactions()));
            },
          ),
        ],
      ),

    );
  }
    
}