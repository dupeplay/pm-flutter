import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pm/Constants.dart';
import 'package:pm/Projects/ProjectList/ProjectListComponents.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Dashboardbody extends StatefulWidget{

  @override
  _DashboardbodyState createState() => _DashboardbodyState();
}

class _DashboardbodyState extends State<Dashboardbody>{

  bool isLoading;
  var userId;
  var fName;
  var lName;
  var userEmail;
  var userDisplay;
  var jobTitle;
  var userAvtar;

  Future<dynamic> getUserInfo() async{

    isLoading = true;
    var user= User();
    var userInfo =  await user.getSharedPreferences();

    setState(() {
      userId = userInfo[0];
      fName = userInfo[1];
      lName = userInfo[2];
      userEmail = userInfo[3];
      userDisplay = userInfo[4];
      jobTitle = userInfo[5];
      userAvtar = fName[0] + lName[0];
      isLoading = false;

    });

  }

  @override
  void initState() {
    // TODO: implement initState
    //test();
    getUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context){

    return Center(
      child: isLoading
      ? CircularProgressIndicator()
      : Container(
        decoration: insertBg(),
        child: ListView(
          children: [

            // User Card
            Container(
              margin: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 30.0),
              child: Row(
                children: [

                  // User Image Conatiner
                  Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: CircleAvatar(
                        radius: 30.0,
                        backgroundColor: Colors.white,
                        child: 
                          (userDisplay != null)
                          ? Image.network(userDisplay, fit: BoxFit.fill,)
                          : Text(userAvtar.toUpperCase()),
                      ),
                    ),
                  ),
                  // User Information Container
                  Flexible(
                    flex: 3,
                    fit: FlexFit.loose,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: RichText(
                        text: TextSpan(
                          text: fName+lName,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0, color: Colors.black54),
                          children: <TextSpan>[
                            TextSpan(text: '\nE-mail: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0, color: Colors.black54)),
                            TextSpan(text: userEmail, style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14.0, color: Colors.black54)),
                            TextSpan(text: '\nJob Title: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0, color: Colors.black54)),
                            TextSpan(text: jobTitle, style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14.0, color: Colors.black54)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

          // Menu Section
          Container(
            child: Column(
              children: [

                // Menu Row 1
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
                  child: Row(
                    children: [

                      // Row 1 Item 1
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            GestureDetector(
                              child: Container(
                                child: CircleAvatar(
                                  radius: 25.0,
                                  backgroundColor: Colors.greenAccent,
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Icon(Icons.developer_board, color: Colors.white,),
                                  ),
                                ),
                              ),
                              onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> ProjectListComponents()));},
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Projects", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 1 Item 2
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.calendar_today, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Calendar", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 1 Item 3
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.message, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Messages", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 1 Item 4
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.timeline, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Timeline", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),


                    ],
                  ),


                ),

                // Menu Row 2
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                  child: Row(
                    children: [

                      // Row 2 Item 1
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.local_hospital, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Leaves", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 2 Item 2
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.move_to_inbox, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Notes", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 2 Item 3
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.schedule, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Timer", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),

                      // Row 2 Item 4
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: Column(
                          children: [
                            
                            // Button Icons
                            Container(
                              child: CircleAvatar(
                                radius: 25.0,
                                backgroundColor: Colors.greenAccent,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.speaker_phone, color: Colors.white,),
                                ),
                              ),
                            ),
                            
                            // Titles
                            Container(
                              child: Text("Broadcast", style: TextStyle(fontSize:12.0, color: Colors.black54)),
                            ),
                          ],
                        ),
                      ),


                    ],
                  ),


                ),
              ],
            ),
          ),

          // Timer Card
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 30.0),
            child: Card(
              color: Colors.greenAccent[200],
              shadowColor: Colors.green[200],
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: Column(
                  
                  children: [

                    // Clockin/Clockout Title
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text("You're currently Clocked Out", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color:Colors.white),),
                    ),

                    // Clock Icon & Button
                    Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 20.0),
                      child: Row(
                        children: [

                          // Clock Icon
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              child: Icon(Icons.timer, size: 80.0, color: Colors.white54,),
                            ),
                          ),

                          // Button
                          Flexible(
                            flex: 3,
                            fit: FlexFit.tight,
                            child: Container(
                              margin: EdgeInsets.fromLTRB(60.0, 0.0, 0.0, 0.0),
                              child: SizedBox(
                                child: RaisedButton(
                                  color: Colors.white,
                                  onPressed: (){},
                                  child: Text("Clock In", style: TextStyle(color:Colors.greenAccent),),
                                  ),
                              )
                            ),
                          ),

                        ],
                      ),
                    ),

                  ],
                ),
              )
            ),
          ),


          // Misc Widget
          Container(
            margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 30.0),
            child: Card(
              color: Colors.greenAccent[200],
              shadowColor: Colors.green[200],
              child: Container(
                margin: EdgeInsets.all(10.0),
                child: Column(
                  
                  children: [

                    // Open Task
                    Container(
                      child: Row(
                        children: [

                          // Open Task Title
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              child: Text("My Open Tasks", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color:Colors.white),),
                            ),
                          ),

                          // No of Open Tasks
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: Card(
                                child: Container(
                                  margin: EdgeInsets.all(5.0),
                                  child: Text(" 0 ", style: TextStyle(color:Colors.greenAccent),),
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),

                    Divider(
                      color: Colors.white,
                      height: 20.0,
                    ),

                    // Events
                    Container(
                      child: Row(
                        children: [

                          // Events Title
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              child: Text("Events Today", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color:Colors.white),),
                            ),
                          ),

                          // No of events
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: Card(
                                child: Container(
                                  margin: EdgeInsets.all(5.0),
                                  child: Text(" 3 ", style: TextStyle(color:Colors.greenAccent),),
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),

                    Divider(
                      color: Colors.white,
                      height: 20.0,
                    ),


                    // Messages
                    Container(
                      child: Row(
                        children: [

                          // Unread Messages
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              child: Text("Unread Messages", style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color:Colors.white),),
                            ),
                          ),

                          // No of Unread Messages
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: Card(
                                child: Container(
                                  margin: EdgeInsets.all(5.0),
                                  child: Text(" 1 ", style: TextStyle(color:Colors.greenAccent),),
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),


                  ],
                ),
              )
            ),
          )


          ],
        ),
      ),
    );
  }

}