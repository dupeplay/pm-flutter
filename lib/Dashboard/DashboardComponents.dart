import 'package:flutter/material.dart';
import 'package:pm/Dashboard/Dashboardbody.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:pm/Constants.dart';
import 'package:pm/Components.dart';
import 'package:pm/DrawerApp.dart';

class DashboardComponents extends StatefulWidget{

  @override
  _DashboardComponentsState createState() => _DashboardComponentsState();
}

class _DashboardComponentsState extends State<DashboardComponents> {

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context){

    return MaterialApp(
      theme: appTheme(),
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Dashboard", style: TextStyle(color:Colors.white),),
          // leading: IconButton(
          //   icon: Icon(Icons.apps, color: Colors.white,),
          //   onPressed: ()=>_scaffoldKey.currentState.openDrawer(),
          // ),
          actions: <Widget>[
            Components(),
          ],
        ),
        // drawer: DrawerApp(),
        body: Dashboardbody(),
      ),
    );
  }
}