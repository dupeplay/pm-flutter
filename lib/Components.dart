import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Login.dart';


// App Bar Components

class Components extends StatefulWidget{

  @override
  _ComponentsState createState() => _ComponentsState();
}

class _ComponentsState extends State<Components>{

  @override
  Widget build(BuildContext context){

  List<String> _actionMenu = ["Settings", "Change Password", "Logout"];

    return Container(
      child: PopupMenuButton(
        icon: Icon(Icons.settings, color:Colors.white),
        onSelected: (value){
          _actionButton(value);
        },
        offset: Offset(10, 80),
        itemBuilder: (BuildContext context) => [
          PopupMenuItem(
            value: 1,
            child: Text(_actionMenu[0], style:TextStyle(color: Colors.green[400], fontSize: 13.0, fontWeight: FontWeight.w700),),
          ),
          PopupMenuItem(
            value: 2,
            child: Text(_actionMenu[1], style:TextStyle(color: Colors.green[400], fontSize: 13.0, fontWeight: FontWeight.w700),),
          ),
          PopupMenuItem(
            value: 3,
            child: Text(_actionMenu[2], style:TextStyle(color: Colors.green[400], fontSize: 13.0, fontWeight: FontWeight.w700),),
          ),
        ],
      ),
    );

  }

  _actionButton(value){
    if(value == 1){
      print("Settings");
    }
    else if(value == 2){
      print("Change Password");
      // Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPassword()));

    }
    else{
      print("logout");
      setState(() async{
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.clear();
        Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
      });
    }
  }
}