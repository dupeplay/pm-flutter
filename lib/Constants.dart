import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


var baseUrl = "http://devproject.apstrix.com/index.php/api/";
var apikey = "79c2e7c0-d254-403e-8b0f-d2085ab22b3c";

// Theme Settings
appTheme(){
    return ThemeData(
      primaryColor: Colors.greenAccent[400],
    );
}

// Display Background Settings
insertBg(){
  return BoxDecoration(
    image: DecorationImage(
      image: AssetImage('assets/images/pm_mainbg.jpg'),
      fit: BoxFit.cover,
    ),
  );
}



class User{

  // int userId;
  // String fName;
  // String lName;
  // var userEmail;
  // var userDisplay;
  // String jobTitle;

  // //User(this.userId, this.fName, this.lName, this.userEmail, this.userDisplay, this.jobTitle);


 Future<List> getSharedPreferences() async{
    
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    List userInfo = [];

      userInfo.add(sharedPreferences.getInt("userId"));
      userInfo.add(sharedPreferences.getString("fname"));
      userInfo.add(sharedPreferences.getString("lname"));
      userInfo.add(sharedPreferences.getString("email"));
      userInfo.add(sharedPreferences.getString("image"));
      userInfo.add(sharedPreferences.getString("jobTitle"));

      return userInfo;

  }

}

