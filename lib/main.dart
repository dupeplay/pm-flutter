import 'package:flutter/material.dart';
import 'package:pm/Constants.dart';
import 'Login.dart';


void main() => runApp(ProjectManager());

class ProjectManager extends StatelessWidget{

  @override
  Widget build(BuildContext context){

    return MaterialApp(

      title: 'Project Management App',
      theme: appTheme(),
      home: Login(),
    );

  }
}